provider "aws" {
#	region = "eu-west-3"
#	region = "eu-central-1"
	region = "us-east-2"
}

resource "aws_instance" "example" {
#	ami = "ami-0cc814d99c59f3789"
#	ami = "ami-09c5ba4f838d8684a"
	ami = "ami-0fb653ca2d3203ac1"
	instance_type = "t2.micro"
	vpc_security_group_ids = [aws_security_group.instance.id]

	user_data = data.template_file.user_data.rendered

#	user_data_replace_on_change = true

	tags = {
		Name = "terraform-example"
	}
}

resource "aws_security_group" "instance" {
	name = var.security_group_name

	ingress {
		from_port = var.server_port
		to_port = var.server_port
		protocol = "tcp"
		cidr_blocks = ["0.0.0.0/0"]
	}
	
	ingress {
		from_port = var.ssh_port
		to_port = var.ssh_port
		protocol = "tcp"
		cidr_blocks = ["0.0.0.0/0"]
	}

#	egress {
#		from_port = var.server_port
#		to_port = var.server_port
#		protocol = "tcp"
#		cidr_blocks = ["0.0.0.0/0"]
#	}
}

data "template_file" "user_data" {
  	template = file("${var.user_data_file}")
}
