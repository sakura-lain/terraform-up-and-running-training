output "public_ip" {
	value = aws_instance.example.public_ip
	description = "The public IP address of the webserver"
}

output "public_dns" {
	value = aws_instance.example.public_dns
	description = "The public DNS of the webserver"
}
