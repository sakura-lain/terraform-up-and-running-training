provider "aws" {
#	region = "eu-west-3"
#	region = "eu-central-1"
	region = "us-east-2"
}

resource "aws_launch_configuration" "example" {
#	image_id = "ami-0cc814d99c59f3789"
#	image_id = "ami-09c5ba4f838d8684a"
	image_id = "ami-0fb653ca2d3203ac1"
	instance_type = "t2.micro"
	security_groups = [aws_security_group.instance.id]

	user_data = <<-EOF
                    #!/bin/bash
                    echo "Hello, World" > index.html
                    nohup busybox httpd -f -p ${var.server_port} &
                    EOF                    

#	user_data = "${file("simple-webserver.sh")}"
	
#	user_data_replace_on_change = true

	lifecycle { 
		create_before_destroy = true
	}
}

resource "aws_autoscaling_group" "example" {
	launch_configuration = aws_launch_configuration.example.name
	vpc_zone_identifier = data.aws_subnet_ids.default.ids
	
	min_size = 2
	max_size = 10
	
	tag {
		key = "Name"
		value = "terraform-asg-example"
		propagate_at_launch = true
	}
}

resource "aws_security_group" "instance" {
	name = var.security_group_name

	ingress {
		from_port = var.server_port
		to_port = var.server_port
		protocol = "tcp"
		cidr_blocks = ["0.0.0.0/0"]
	}
	
	ingress {
		from_port = var.ssh_port
		to_port = var.ssh_port
		protocol = "tcp"
		cidr_blocks = ["0.0.0.0/0"]
	}

#	egress {
#		from_port = var.server_port
#		to_port = var.server_port
#		protocol = "tcp"
#		cidr_blocks = ["0.0.0.0/0"]
#	}
}

data "aws_vpc" "default" {
	default = true
}

data "aws_subnet_ids" "default" {
	vpc_id = data.aws_vpc.default.id
}
