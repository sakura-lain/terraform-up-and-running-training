provider "aws" {
#	region = "eu-west-3"
#	region = "eu-central-1"
	region = "us-east-2"
}

resource "aws_instance" "example" {
#	ami = "ami-0cc814d99c59f3789"
#	ami = "ami-09c5ba4f838d8684a"
 	ami = "ami-0fb653ca2d3203ac1"
	instance_type = "t2.micro"
	vpc_security_group_ids = [aws_security_group.instance.id]

	user_data = <<-EOF
                    #!/bin/bash
                    echo "Hello, World" > index.html
                    nohup busybox httpd -f -p ${var.server_port} &
                    EOF
                    
#	user_data_replace_on_change = true
	
	tags = {
		Name = "terraform-example"
	}
}

resource "aws_security_group" "instance" {
	name = "terraform-example-instance"
	
	ingress {
		from_port = var.server_port
		to_port = var.server_port
		protocol = "tcp"
		cidr_blocks = ["0.0.0.0/0"]
	}
}

variable "server_port" {
	description = "The port the server will use for HTTP requests"
	type = number
	default = 8080
}

output "public_ip" {
	value = aws_instance.example.public_ip
	description = "The public IP address of the webserver"
}

output "public_dns" {
	value = aws_instance.example.public_dns
	description = "The public DNS of the webserver"
}
